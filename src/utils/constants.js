export const COLUMNS = {
	Merchant: 'Merchant',
	Note: 'Note',
	Amount: 'Booked amount',
	Currency: 'Account Currency',
	Message: 'Message',
	TransactionType: 'Transaction type',
	NameOfAccount: 'Name of Account',
	Date: 'Booking Date'
}


export const CATEGORIES = {
	LUNCHES: 'Lunch',
	SUPERMARKETS: 'Supermarkets',
	ENTERTAINMENT: 'Entertainment',
	HOUSE: 'House',
	COSMETICS: 'Cosmetics',
	CHEMICALS: 'Chemicals',
	HEALTH: 'Health',
	RESTAURANTS: 'Restaurants',
	CLOTHES: 'Clothes',
	TRANSPORT: 'Transport',
	COMMUNICATION: 'Phone and internet',
	CASHWITHDRAWAL: 'Cash Withdrawal',
	ELECTRONICS: 'Electronics',
	INTERNET_SHOPS: 'Internet shops',
	SAVINGS: 'Savings',
	OTHER: 'Other'
}

export const DESCRIPTION_COLUMNS = [
	COLUMNS.Merchant,
	COLUMNS.Note,
	COLUMNS.Message,
]

export const CATEGORIES_EXCLUDED_FROM_REPORT = [
	CATEGORIES.SAVINGS
]

export const COLUMNS_DESCRIBING_UNKNOWN_PAYMENTS = [
	COLUMNS.Merchant,
	COLUMNS.Note,
	COLUMNS.Message,
	COLUMNS.NameOfAccount,
]

export const UNAVAILABLE = 'N/A'

export const REPORT_DATE_FORMAT = 'LL'
export const PARSE_DATE_FORMAT = 'DD.MM.YYYY'