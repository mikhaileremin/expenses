import {COLUMNS, CATEGORIES} from './constants';

export const MATCH_BY_CATEGORIES = [
	{
		category: CATEGORIES.LUNCHES,
		match: [
			'food garden'
		]
	},
	{
		category: CATEGORIES.SUPERMARKETS,
		match: [
			'tesco',
			'albert',
			'Žabka',
			'Zabka',
			'Billa',
			'potravin',
			'SAMOOBSLUHA',
			'food',
			'grizly',
			'grocery',
			'RELAY',
			'lastochka',
			'speciality',
			'Lidl',
			'RELAY',
			'minimarket',
			'Nespresso',
			'OVOCE',
			'ZELENINA'
		]
	},
	{
		category: CATEGORIES.INTERNET_SHOPS,
		match: [
			'aliexpress'
		]
	},
	{
		category: CATEGORIES.COSMETICS,
		match: [
			'Rossmann',
			'BIOOO',
			'Europarfemy',
			'dm drogerie',
			'teta drogerie'
		]
	},
	{
		category: CATEGORIES.HEALTH,
		match: [
			'Dr. Max',
			'lekarna',
			'BENU',
			'Pilulka',
		]
	},
	{
		category: CATEGORIES.RESTAURANTS,
		match: [
			'kfc',
			'BAGETERIE',
			'restaurace',
			'uber eat',
			'Burrito',
			'MCDONALDS',
			'McDonald\'s',
			'pho ',
			' pho',
			'La Focacceria',
			'kebab',
			'pizza',
			'WAFWAF',
			'DAMEJIDLO',
			'Thai Thai',
			'DAI MARINAI',
			'GALERIE PIVA',
			'cafeteria',
			'cafe',
			'Spicymama',
			'brambor'
		]
	},
	{
		category: CATEGORIES.TRANSPORT,
		match: [
			'bolt',
			'uber',
			'cd.cz',
			'idos.cz',
			'I. P. PAVLOVA PM',
			'BUDGET AT CAR',
			'Lítačka',
			'Litacka',
			'OPERATOR ICT', // letacka
			'MOL ',
			'BENZINA'
		]
	},
	{
		category: CATEGORIES.CLOTHES,
		match: [
			'tezenis',
			'h&m',
			'H & M',
			'Deichmann',
			'C&A',
			'zara',
			'Decathlon',
			'SD.COM AVION',
			'new yorker',
			'reserved',
			'adidas',
			'reebok',
			'sportisimo',
			'wildberries'
		]
	},
	{
		category: CATEGORIES.HOUSE,
		match: [
			'byt 13',
			'pre.cz'
		]
	},
	{
		category: CATEGORIES.ENTERTAINMENT,
		match: [
			'CINEMA',
			'rekola',
			'lime',
			'FREEBIKE'
		]
	},
	{
		category: CATEGORIES.COMMUNICATION,
		match: [
			'upc qr',
			'vodafone'
		]
	},
	{
		category: CATEGORIES.ELECTRONICS,
		match: [
			'ALZA',
			'data art',
			'datart',
			'mall.cz'
		]
	}
]

export const MATCH_SPECIAL = [
	{
		category: CATEGORIES.CASHWITHDRAWAL,
		match: [
			'ATM withdrawal',
			'ATM'
		],
		columns: [COLUMNS.TransactionType]
	},
	{
		category: CATEGORIES.SAVINGS,
		match: [
			'Mikhail Eremin'
		],
		columns: [COLUMNS.NameOfAccount]
	},
	{
		category: CATEGORIES.COMMUNICATION,
		match: [
			'Card maintenance fee',
			'Summary item for account'
		],
		columns: [COLUMNS.TransactionType]
	}
]


