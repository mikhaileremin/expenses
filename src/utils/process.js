import moment from 'moment'
import pick from 'lodash/pick'
import {matchOnArray} from './helpers'
import {
	COLUMNS,
	CATEGORIES,
	REPORT_DATE_FORMAT, PARSE_DATE_FORMAT,
	UNAVAILABLE,
	DESCRIPTION_COLUMNS,
	CATEGORIES_EXCLUDED_FROM_REPORT,
	COLUMNS_DESCRIBING_UNKNOWN_PAYMENTS
} from './constants'
import {MATCH_BY_CATEGORIES, MATCH_SPECIAL} from './match'


export const process = (csvs = []) => {
	const summary = {}
	const detailed = {}
	const data = csvs.reduce((acc, csv) => { return [...acc, ...csv]}, [])
	let total = 0
	let savings = 0

	for (let row of data) {
		const category = getCategory(row)
		const amount = getAmountCzk(row)
		if (amount === 0) {
			continue
		}
		// Сбережения не идут в summary
		if (isSavingsCategory(category)) {
			savings += amount
			continue
		}

		// Специальные категории которые не идут в саммари и никак не учитываются
		if (isExcludedCategory(category)) {
			continue
		}

		total += amount

		const shortDescription = getPaymentShortDescription(row)

		if (detailed[category]) {
			detailed[category].push(shortDescription)
		} else {
			detailed[category] = [shortDescription]
		}

		if (summary[category]) {
			summary[category] = summary[category] + amount
		} else {
			summary[category] = amount
		}
	}

	return {
		summary,
		total,
		savings,
		detailed,
		dateFrom: getDateFrom(data),
		dateTo: getDateTo(data),
	}
}

const getCategory = (row) => {
	// match special
	for (let c of MATCH_SPECIAL) {
		for (let col of c.columns) {
			if (row[col] && matchOnArray(row[col], c.match)) {
				return c.category
			}
		}
	}

	// match others
	for (let c of MATCH_BY_CATEGORIES) {
		for (let col of DESCRIPTION_COLUMNS) {
			if (row[col] && matchOnArray(row[col], c.match)) {
				return c.category
			}
		}
	}
	return CATEGORIES.OTHER
}


const getPaymentShortDescription = (row) => {
	const info = pick(row, COLUMNS_DESCRIBING_UNKNOWN_PAYMENTS)
	const description = []
	for (let col of COLUMNS_DESCRIBING_UNKNOWN_PAYMENTS) {
		if (info[col] && !description.includes(info[col])) {
			description.push(info[col])
		}
	}
	return {
		description: description.join(', '),
		amount: getAmountCzk(row),
		date: moment(row[COLUMNS.Date], PARSE_DATE_FORMAT).format(REPORT_DATE_FORMAT)
	}
}

const isExcludedCategory = (category) => {
	return CATEGORIES_EXCLUDED_FROM_REPORT.includes(category)
}

const isSavingsCategory = (category) => {
	return CATEGORIES.SAVINGS === category
}

const getDateTo = (data) => {
	const date = data[0] && data[0][COLUMNS.Date]
	if (!date) return UNAVAILABLE

	return moment(date, PARSE_DATE_FORMAT).format(REPORT_DATE_FORMAT)
}

const getDateFrom = (data) => {
	const date = data[data.length - 1] && data[data.length - 1][COLUMNS.Date]
	if (!date) return UNAVAILABLE
	return moment(date, PARSE_DATE_FORMAT).format(REPORT_DATE_FORMAT)
}

const getAmountCzk = (row) => {
	const amountString = row[COLUMNS.Amount].replace(' ', '')
	if (!amountString) {
		return 0
	}
	const parsedAmount = parseFloat(amountString)
	if (!parsedAmount) return 0

	// for incoming return nothing
	if (parsedAmount > 0) {
		return 0
	}

	return parsedAmount * -1
}
