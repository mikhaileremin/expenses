// test if string contains any substrings from array
import escapeRegExp from 'lodash/escapeRegExp';
import csv from 'csvtojson';

export const matchOnArray = (str = '', arr = ['']) => {
	const escapedArr = arr.map(escapeRegExp)
	return new RegExp(escapedArr.join('|'), 'gi').test(str)
}


export const fromCSV = async (text) => {
	return await csv({
		delimiter: [";",",","|","$",'\t']
	}).fromString(text)
}
