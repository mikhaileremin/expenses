import Vue from 'vue';

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

// Vue.use(Vuetify, {
//   iconfont: 'md',
// })

Vue.use(ElementUI, {
	iconfont: 'md',
})