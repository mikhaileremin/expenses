import Vue from 'vue'
import AppLayout from './AppLayout.vue'
import vuetify from './plugins/vuetify';
import './plugins/charts';
import './plugins/elementui';

Vue.config.productionTip = false

new Vue({
  vuetify,
  render: h => h(AppLayout)
}).$mount('#app')
